# botcast

Unlike traditional Podcast, Botcast is not hosted by human, They
are written as showscript which will be read by your device. (Also
known as Text-To-Speech Technology). When a user subscibe a
botcast, they can customize the show according to what they
prefer. The show will fetch and insert related materials into the
script when the user try to play it, and the script will be read by
computer voices.

# showscript

Imagine there‘s a radio station, shows are hosted by talking robots.
You tell them what you like, and they boardcast what you needs. In
Clouds.FM, you choose the shows you like to subscribe, tell them
when should be boardcast, what should be boardcast. Then they‘ll
collect required materials to boardcast a show.

For Example: You subscibe a Weather Report show to boardcast on
every morning. When the time come, A Notification pops out,
Telling you should listen to Morning Channel. The Show knows
where you live, So it‘ll will boardcast local weather report for you.

# voices

# materials

# conditions
