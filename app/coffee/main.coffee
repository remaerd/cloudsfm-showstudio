# Main 主要用于设置 RequireJS 的各个参数，并生成一个实体 Application 类

require.config

  baseUrl: 'scripts/components'

  paths:
    app          : '../app'
    templates    : '../../templates'
    nls          : '../app/nls'

    text         : 'requirejs-text/text'
    i18n         : 'requirejs-i18n/i18n'
    cookie       : 'zepto-cookie/zepto.cookie'
    backbone     : 'backbone/backbone'
    localStorage : 'backbone.localStorage/backbone.localStorage'
    markdown     : 'markdown/lib/markdown'
    lodash       : 'lodash/dist/lodash.underscore'
    zepto        : 'zepto/zepto'

  shim:
    'app/router' : ['backbone']

    cookie:
      deps    : ['zepto']

    backbone:
      deps    : ['lodash','zepto']
      exports : 'Backbone'

    localStorage: ['backbone']

    lodash:
      exports: '_'

    zepto:
      exports: '$'

require ['app/application'], (Application) ->

  app = new Application()
