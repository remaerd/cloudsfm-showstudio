define ['backbone','app/router','app/views/layout'
        'app/models/distributor','cookie'],(Backbone,Router,AppView,Distributor) ->

# Application 类主要用于创建软件的几个核心组件

  class Application

    # 创建并设置程序的几个核心组件，监听 Mozilla Persona 登录／登出，并根据浏览器 Cookies 进行自动登录

    constructor: ->
      appView = new AppView()
      router = new Router()

      Backbone.history.start()
      $.ajaxSettings.beforeSend = (xhr) -> xhr.withCredentials = true

      navigator.id.watch
        onlogin: (assertion) -> Distributor.instance().login('assertion':assertion)
        onlogout: -> appView.welcome()

      if $.fn.cookie('distributor') then Distributor.instance().login()
