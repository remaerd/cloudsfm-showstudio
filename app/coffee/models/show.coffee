define ['backbone'], (Backbone) ->

# Show 采用单件模式设计，主要负责 “节目” 部分的数据逻辑。与服务器后端 CAPI 进行数据通信

  class Show extends Backbone.Model

    idAttribute: '_id'

    urls:
      'create': ['GET', cors + 'shows/new',null]
      'delete': ['GET', cors + '',null]

    sync: (method,model,options) ->
      options = options || {}
      options.url = @urls[method][1]
      options.type = @urls[method][0]
      options.data = @urls[method][2]
      Backbone.sync(method,model,options)

  class Shows extends Backbone.Collection
    model: Show
    url: cors + 'shows'

  class ShowsInstance
    @_instance = null

    @instance: ->
      @_instance ?= new Shows()
      return @_instance

