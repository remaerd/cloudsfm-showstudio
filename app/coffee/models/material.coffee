define ['backbone'], (Backbone) ->

# Material 采用单件模式设计，主要负责 “节目素材” 部分的数据逻辑。与服务器后端 CAPI 进行数据通信

	class Material extends Backbone.Model

    idAttribute: '_id'

    urls:
      'create': ['GET', cors + 'materials/new',null]
      'delete': ['GET', cors + 'materials/:id/destroy',null]

    sync: (method,model,options) ->
      options = options || {}
      options.url = @urls[method][1]
      options.type = @urls[method][0]
      options.data = @urls[method][2]
      Backbone.sync(method,model,options)

	class Materials extends Backbone.Collection
    model: Material
    url: cors + 'materials'

  class MaterialsInstance
    @_instance = null

    @instance: ->
      @_instance ?= new Materials()
      return @_instance

