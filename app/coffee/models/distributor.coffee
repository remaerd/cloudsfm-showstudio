define ['backbone','app/router','cookie'], (Backbone,Router) ->

# Distributor 采用单件模式设计，主要负责 “节目发布者” 部分的数据逻辑。与服务器后端 CAPI 进行数据通信

  class Distributor extends Backbone.Model

    idAttribute: '_id'
    isLoggedIn: false

    # 创建一个发布者 Model，如 Arguements 中包含 Assertion 参数，通过 AJAX 访问 CAPI 的登录接口。
    # 如没有参数，通过 AJAX 与浏览器 Cookies 访问 CAPI 的自动登录接口。

    login: (arguements) ->
      if arguements
        data = arguements
        url  = cors + 'distributor/login'
      else url = cors + 'session'
      $.ajax
        url      : url
        data     : data
        dataType : 'json'
        success: (responseData) =>
          if data
            $.fn.cookie('distributor',responseData.distributor._id)
            distributorData = responseData.distributor
          else distributorData = responseData

          # 如果账号状态正常，设置本 Model 的参数
          if distributorData.status is 'actived'
            @isLoggedIn = true
            @.id = distributorData._id
            @.set 'email', distributorData.email
            @.set 'name', distributorData.name
            Router.instance().navigate '#/shows',
              trigger: true
          else
            Router.instance().alert('alert_suspended_distributor')

        error: (xhr,type) ->

          # 如出现 401 错误，返回到登录界面
          if xhr.status is 401 then Router.instance().navigate('')

  class DistributorInstance
    @_instance = null

    @instance: ->
      @_instance ?= new Distributor()
      return @_instance
