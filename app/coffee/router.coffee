define ['backbone'],(Backbone) ->

# Router 是一个采用了单件设计模式的类，主要负责软件导航

  class PrivateRouter extends Backbone.Router

    prevRoute: null

    routes:
      ''                           : 'welcome'
      'distributor'                : 'distributor'
      'shows'                      : 'shows'
      'shows/new'                  : 'new'
      'shows/:id'                  : 'dashboard'
      'shows/:id/script/:language' : 'script'
      'materials'                  : 'materials'
      'materials/:id'              : 'material'
      'market'                     : 'market'
      'help/:type'                 : 'help'

    loading : -> @.trigger('loading')
    alert: (errorCode) ->  @.trigger('alert',errorCode)

  class Router

    @_instance = null

    @instance: ->
      @_instance ?= new PrivateRouter()
      return @_instance
