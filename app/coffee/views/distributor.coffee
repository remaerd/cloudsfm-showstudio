define ['backbone','text!templates/distributor.html','i18n!nls/ui'], (Backbone,template,locale) ->

# DistributorView 用于显示发布者设置界面

  class DistributorView extends Backbone.View

    el: 'div.main'

    events:
      'click #done' : 'done'

    template: _.template(template)

    initialize: (arguements) ->
      @error = arguements.errorCode
      @.render()

    render: ->
      @.$el.html @template
        'distributor_balance'        : locale.distributor_balance
        'distributor_history'        : locale.distributor_history
        'distributor_deposit'        : locale.distributor_deposit
        'distributor_withdrawal'     : locale.distributor_withdrawal
        'distributor_change_persona' : locale.distributor_change_persona
        'distributor_change_paypal'  : locale.distributor_change_paypal
        'distributor_change_name'    : locale.distributor_change_name

    done: ->
      navigator.id.request
        siteName: 'ShowStudio'
