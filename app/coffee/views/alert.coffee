define ['backbone','text!templates/alert.html','i18n!nls/alert'], (Backbone,template,alert) ->

# AlertView 是一个全屏显示的界面，用于显示错误状态。

  class AlertView extends Backbone.View

    el: 'div.modal'

    events:
      'click #done' : 'done'

    template: _.template(template)

    initialize: (arguements) ->
      @error = arguements.errorCode
      @.render()

    render: ->
      @.$el.html @template
        'alert_message' : alert[@error]

    done: ->
      navigator.id.request
        siteName: 'ShowStudio'
