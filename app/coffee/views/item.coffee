define ['backbone','app/router','text!templates/item.html'], (Backbone, Router, template) ->

  class ItemView extends Backbone.View

    className: 'item'

    events:
      'click' : 'click'

    template: _.template(template)

    initialize: (arguements) ->

      if arguements.item
        @item = arguements.item
        @type = @item.constructor.name.toLowerCase() + 's'
      else @type = arguements.type
      @render()

    render: ->
      console.log @item
      title = @item.get('title') if @item
      @.$el.html @template
        'item_title': title

    click: ->
      if @item then Router.instance().navigate('#/' + @type + '/' + @item.id)
      else Router.instance().navigate('#/' + @type + '/new')
