define ['backbone','text!templates/show_dashboard.html','i18n!nls/ui'], (Backbone, template, locale) ->

  class DashboardView extends Backbone.View

    pinned: true
    el: 'div.main'
    template: _.template(template)

    initialize: (arguements) ->

      @item = arguements.item
      @render()

    render: ->
      @.$el.html @template
        'dashboard_status_private' : locale.dashboard_status_private
        'dashboard_status_public'  : locale.dashboard_status_public
        'dashboard_push'           : locale.dashboard_push
        'dashboard_destroy'        : locale.dashboard_destroy
        'dashboard_daily'          : locale.dashboard_daily
        'dashboard_weekly'         : locale.dashboard_weekly
        'dashboard_monthly'        : locale.dashboard_monthly

      $('input#show_uid').attr('placeholder',locale.dashboard_uid)
      console.debug @item
      if @item.get('reminder_frequency').length is 0
        $('a.button#show_reminder').html(locale.reminder_type_no)
      if @item.get('cover_iamge_url') then $('img#show_image').attr('src',@item.get('cover_iamge_url'))
      else $('div.no_image').css('display':'block')
