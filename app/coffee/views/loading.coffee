define ['backbone','text!templates/loading.html'], (Backbone,template) ->

# LoadingView 是一个全屏显示的界面，用于显示载入状态。

  class LoadingView extends Backbone.View

    el: 'div.modal'

    template: _.template(template)

    initialize: ->
      @.render()

    render: ->
      @.$el.html @template
