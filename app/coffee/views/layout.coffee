define ['backbone','app/router','app/models/distributor','app/models/show','app/models/material'
        'app/views/loading','app/views/welcome','app/views/distributor','app/views/show_dashboard'
        'app/views/show_script','app/views/show_new','app/views/alert','app/views/list','app/views/help'
        'text!templates/layout.html','i18n!nls/ui']
        , (Backbone,Router,Distributor,Show,Materials,LoadingView,WelcomeView,DistributorView,DashboardView,ScriptView,NewView,AlertView,ListView,HelpView,template,locale) ->

# LayoutView 是整个软件操作界面的底层。负责管理软件的各类 View，并传送对应的 Model 模型

  class LayoutView extends Backbone.View

    el           : 'body'
    template     : _.template(template)
    mainView     : null
    previousView : null

    events:
      'click span.left a.button'   : 'leftClicked'
      'click span.right a.button'  : 'rightClicked'
      'click span.center a.button' : 'buttonClicked'

    initialize: ->
      @.listenTo(Router.instance(), 'loading', @loading)
      @.listenTo(Router.instance(), 'alert', @alert)
      @.listenTo(Router.instance(), 'route', @route)
      @.render()

    resize: ->
      width = ($(window).width() * 0.25) - 2
      $('div.item').css
        height : width
        width  : width

    render: ->
      @.$el.html @template
        'button_left'      : null
        'button_dashboard' : locale.button_dashboard
        'button_script'    : locale.button_script
        'button_shows'     : locale.button_shows
        'button_materials' : locale.button_materials
        'button_market'    : locale.button_market
        'button_advertise' : locale.button_advertise
        'button_botcast'   : locale.button_botcast
        'button_podcast'   : locale.button_podcast
        'button_ads'       : locale.button_ads
        'button_right'     : null

      $(window).resize =>
        clearTimeout(done)
        done = setTimeout(@resize, 100)

#   判断用户是否处于同一类操作界面，根据情况替换导航栏按钮，并更换老界面实体

    isCurrentView: (type) ->
      # 如果更换了界面，删掉老界面的委托事件，并从 DOM 中删掉界面
      value = true
      if @previousView isnt null and (@previousView != type or type is 'list')
        @mainView.undelegateEvents()
        @mainView.$el.children().remove()
        value = true
      # 如果更换了界面，或第一次打开软件，对导航栏的按钮进行设置
      if @previousView is null or @previousView != type
        $('span.tabs').hide()
        $('span.tabs.' + type).show()
        switch type
          when 'list'
            $('div.topbar span.left a.button').html(locale.button_help)
            $('div.topbar span.right a.button').html(Distributor.instance().get('name'))
            $('div.topbar span.right a.button').attr('href', null)
          when 'help'
            $('div.topbar span.left a.button').html(locale.button_done)
            $('div.topbar span.right a.button').html(locale.button_exlimited)
            $('div.topbar span.right a.button').attr('href','http://extremelylimited.com')
            $('div.topbar span.right a.button').attr('target','_blank')
          when 'show'
            $('div.topbar span.left a.button').html(Distributor.instance().get('name'))
            $('div.topbar span.right a.button').html(locale.button_save)
          when 'distributor'
            $('span.tabs.distributor').show()
            $('span.tabs.distributor').html('<a class="button">' + Distributor.instance().get('name') + '</a>')
            $('div.topbar span.right a.button').html(locale.button_done)
            $('div.topbar span.left a.button').html(locale.button_logout)
      @previousView = type
      return value

#   根据链接跳转到软件的各个操作界面

    route: (path,params) ->
      $('a.button').removeClass('active')
      if path is 'welcome'
        if Distributor.instance().isLoggedIn then Router.instance().navigate('#/shows')
        else @welcome()
      else
        if Distributor.instance().isLoggedIn
          switch path
            when 'shows' then @list(Show.instance())
            when 'materials' then @list(Materials.instance())
            when 'distributor' then @distributor()
            when 'dashboard' then @dashboard(params[0])
            when 'script' then @script(params[0],params[1])
            when 'help' then @help(params[0])
            when 'new' then @newShow()
          if path is 'help' then $('a.button#' + params[0]).addClass('active')
          else $('a.button#' + path).addClass('active')
        else @welcome()

        if @mainView
          if @mainView.pinned then $('div.sidebar').addClass('active')
          else
            $('div.sidebar').removeClass('active')
            $('div.sidebar-trigger').on 'mouseover', ->
              $('div.sidebar').addClass('active')
              $('div.sidebar-trigger').hide()
              $('div.sidebar').on 'mouseout', ->
                $('div.sidebar').removeClass('active')
                $('div.sidebar-trigger').show()

    list: (collections) ->

      if collections.isFetched is undefined
        @loading()
        # 从服务器加载发布者的节目／素材
        collections.fetch
          success: (items) =>
            collections.isFetched = true
            @toggleModal(false)
            @listView(collections)
          error: (xhr,type) =>
            @alert('alert_fetch_failed')
      else @listView(collections)

    listView: (collections) ->

      @.isCurrentView('list')
      @mainView = new ListView(collections: collections)
      @resize()

    help: (type) ->
      if @.isCurrentView('help') then @mainView = new HelpView(type:type)

    dashboard: (id) ->
      @currentShowId = id
      if @.isCurrentView('show') then @mainView = new DashboardView(item:Show.instance().get(id))

    script: (id,language) ->
      @currentShowId = id
      if @.isCurrentView('show')
        @mainView = new ScriptView
          item     : Show.instance().get(id)
          language : language

    distributor: ->
      if @.isCurrentView('distributor') then @mainView = new DistributorView(item:Show.instance())

    newShow: ->
      # if @.isCurrentView('new') then @mainView = new NewView()
      console.log 'Hello'
      Show.instance().create {},
        success: (item) =>
          console.log item
          @toggleModal(false)
          # @dashboard
        error: (xhr,type) =>
          @alert('alert_fetch_failed')

    leftClicked: ->
      switch @previousView
        when 'list' then Router.instance().navigate('#/help/botcast')
        when 'distributor' then @logout()
        else Router.instance().navigate('#/shows')

    rightClicked: ->
      switch @previousView
        when 'list' then Router.instance().navigate('#/distributor')
        when 'distributor' then Router.instance().navigate('#/shows')
        when 'help' then return
        else @mainView.save()

    buttonClicked: (button) ->
      switch button.target.id
        when 'shows' then Router.instance().navigate('#/shows')
        when 'materials' then Router.instance().navigate('#/materials')
        when 'botcast' then Router.instance().navigate('#/help/botcast')
        when 'podcast' then Router.instance().navigate('#/help/podcast')
        when 'ads' then Router.instance().navigate('#/help/advertise')
        when 'dashboard' then Router.instance().navigate('#/shows/' + @currentShowId)
        when 'script' then Router.instance().navigate('#/shows/' + @currentShowId + '/script/eng')

    logout: ->
      # 删除 Cookie 并打开登录界面
      $.fn.cookie('distributor', null)
      Router.instance().navigate('')

#   打开／隐藏全屏窗口
    toggleModal: (flag) ->
      if flag
        $('div.modal').show()
        $('div.modal a.button.close').on 'click' , -> @toggleModal(false)
      else
        $('div.modal').hide()
        @modalView.undelegateEvents()
        $('div.modal a.button.close').off('click')

    welcome: ->
      @modalView = new WelcomeView()
      @toggleModal(true)

    loading: ->
      @modalView = new LoadingView()
      @toggleModal(true)

    alert: (errorCode) ->
      @modalView = new AlertView(errorCode:errorCode)
      @toggleModal(true)

