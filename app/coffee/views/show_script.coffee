define ['backbone','text!templates/show_script.html','i18n!nls/ui'], (Backbone, template, locale) ->

  class DashboardView extends Backbone.View

    el: 'div.main'
    template: _.template(template)

    initialize: (arguements) ->

      @item = arguements.item
      @render()

    render: ->
      @.$el.html @template
