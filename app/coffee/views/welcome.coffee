define ['backbone','text!templates/welcome.html','i18n!nls/ui'], (Backbone,template,locale) ->

# AlertView 是一个全屏显示的界面，用于显示未登录界面。

  class WelcomeView extends Backbone.View

    el: 'div.modal'

    events:
      'click #login' : 'login'
      'click #demo'  : 'demo'

    template: _.template(template)

    initialize: ->
      @.render()

    render: ->
      @.$el.html @template
        'welcome_welcome' : locale.welcome_welcome
        'welcome_login'   : locale.welcome_login
        'welcome_demo'    : locale.welcome_demo

    login: ->
      navigator.id.request
        siteName: 'ShowStudio'

    demo: ->
      alert 'Hello World'
