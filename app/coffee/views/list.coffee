define ['backbone','app/views/item','text!templates/list.html'], (Backbone,ItemView,template) ->

# ListView 用于显示节目／素材等列表界面

  class ListView extends Backbone.View

    pinned: false
    el: 'div.main'

    template: _.template(template)

    initialize: (arguements) ->
      @collections = arguements.collections
      @type = @collections.constructor.name.toLowerCase()
      @render()

    render: ->

      @.$el.html @template

      for item in @collections.models
        itemView = new ItemView(item:item)
        $('div.items').append(itemView.el)

      newItemView = new ItemView(type:@type)
      $('div.items').append(newItemView.el)
