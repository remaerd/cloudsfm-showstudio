define ['backbone','text!templates/help.html'], (Backbone,template) ->

# ListView 用于显示节目／素材等列表界面

  class HelpView extends Backbone.View

    el: 'div.main'

    template: _.template(template)

    initialize: (arguements) ->
      @render()
      @load(arguements.type)

    load: (type) ->
      url = '/documents/root/help-' + type + '.html'
      $('div.content').load(url)

    render: ->
      @.$el.html @template
