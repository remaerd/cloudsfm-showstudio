module.exports = (grunt) ->

  pkg = require './package.json'
  hostname = 'studio.dev'
  paths =
    temp      : './temp'
    build     : './build'
    app       : './app'
    coffee    : './app/coffee'
    slim      : './app/slim'
    sass      : './app/sass'
    assets    : './app/assets'
    bower     : './temp/scripts/components'
    styles    : './temp/styles'
    scripts   : './temp/scripts'
    markdown  : './app/md'
    templates : './temp/templates'
    documents : './temp/documents'
    baseUrl   : './temp/scripts'
    name      : './app/main.js'
    config    : './temp/scripts/app/config.js'
    output    : './build/scripts/app/main.js'

  grunt.initConfig
    pkg: pkg

    uglify:
      build:
        src: paths.temp

    slim:
      index:
        files: [
          expand : true
          cwd    : paths.app
          src    : ['index.slim']
          dest   : paths.temp
          ext    : '.html'
        ]
      templates:
        files: [
          expand : true
          flatten: true
          cwd    : paths.slim
          src    : ['{,*/}*.slim']
          dest   : paths.templates
          ext    : '.html'
        ]

    compass:
      dev:
        options:
          importPath : paths.sass
          sassDir    : paths.sass
          cssDir     : paths.styles
      build:
        options:
          environment : 'production'
          importPath  : paths.sass
          sassDir     : paths.sass
          cssDir      : paths.build + '/styles'

    coffee:
      files:
        expand : true
        cwd    : paths.coffee
        dest   : paths.scripts + '/app'
        src    : ['{,*/}*.coffee', 'nls/{,*/}*.coffee']
        ext    : '.js'

    markdown:
      help:
        files: [
          expand : true
          cwd    : paths.markdown
          dest   : paths.documents
          src    : '{,*/}*.md'
          ext    : '.html'
        ]
        options:
          template: 'template.html'
          markdownOptions:
            gfm: false

    concat:
      css:
        src: []

    notify_hooks:
      options:
        enabled                  : true
        max_jshint_notifications : 3
        title                    : pkg.name

    notify:
      build:
        options:
          title   : pkg.name
          message : 'Created a build for production environment'
      dev:
        options:
          title   : pkg.name
          message : 'Development Server is Ready. Slim, Sass and coffee files will be compile automatically'

    watch:
      options:
        livereload: true
        debounceDelay : 250
      templates:
        files: [paths.slim + '/**/*.slim', paths.app + '/index.slim']
        tasks: ['slim']
      scripts:
        files: [paths.coffee + '/**/*.coffee']
        tasks: ['coffeelint','coffee']
      styles:
        files: [paths.sass + '/**/*.sass',paths.sass + '/**/*.scss']
        tasks: ['compass:dev']
      documents:
        files: [paths.markdown + '/**/*.md',paths.markdown + '/**/*.markdown']
        tasks: ['markdown']

    coffeelint:
      coffee:
        files:
          src: [paths.coffee + '**/*.coffee']
        options:
          'no_tabs':
            level: 'ignore'
          'max_line_length':
            level: 'ignore'
          'no_trailing_whitespace':
            level: 'error'

    cssmin:
      build:
        expand : true
        cwd    : paths.temp
        src    : ['*.css', '!*.min.css']
        dest   : paths.build
        ext    : '.min.css'

    imagemin:
      temp:
        options:
          optimizationLevel: 3
        files: [
          expand : true
          cwd    : paths.temp
          src    : ['assets/*.{png,jpg,gif}']
          dest   : paths.temp
        ]
      build:
        options:
          optimizationLevel: 3
        files:
          expand : true
          cwd    : paths.image
          src    : ['**/*.{png,jpg,gif}']
          dest   : paths.build

    clean:
      temp  : [paths.temp]
      build : [paths.build]

    requirejs:
      options:
        appDir         : paths.baseUrl
        # mainConfigFile : paths.config
      build:
        options:
          dir : paths.output
          name: paths.name

    bower:
      build:
        rjsConfig: './temp/scripts/app/config.js'

    copy:
      temp:
        files: [
          expand: true
          cwd: paths.assets
          src: ['**/*.*']
          dest: paths.temp
        ]

    connect:
      temp:
        options:
          port       : 8888
          base       : paths.temp
          livereload : true
      build:
        options:
          port       : 8001
          base       : paths.build
          livereload : true

    concurrent:
      compile :
        tasks: ['slim','coffee','compass:dev','markdown']
        options:
          logConcurrentOutput : true
      minify  : ['cssmin','requirejs:build','imagemin:temp']

  for name of pkg.devDependencies when name.substring(0,6) is 'grunt-' then grunt.loadNpmTasks(name)

  grunt.task.run('notify_hooks')

  grunt.registerTask 'dev', ['coffeelint','concurrent:compile']
  grunt.registerTask 'build', ['dev','bower:build','concurrent:minify','notify:build','connect:build']
  grunt.registerTask 'default', ['dev','imagemin:temp','copy:temp','notify:dev','connect:temp','watch']
  grunt.registerTask 'clean', ['clean']
